'use client';

import { GlobalContext } from '@/context';
import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';

export default function Home() {
  const { isAuthUser } = useContext(GlobalContext);
  console.log(isAuthUser);
  const router = useRouter();

  return (
    <main
      className="h-screen w-screen"
      style={{
        backgroundImage:
          "url('https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fimg2.jpg?alt=media&token=29847943-12a1-497f-b8be-436e13017dc7')",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
      }}
    >
      <div className="grid max-w-screen-xl px-4 py-8 mx-auto lg:gap-8 xl:gap-0 lg:py-16 lg:grid-cols-12">
        <div className="mr-auto lg:col-span-6 lg:mr-16">
          <h1
            className="max-w-2xl mb-6 mt-10 leading-none md:text-5xl xl:text-6xl"
            style={{
              color: '#250A0A',
              fontSize: 56,
              fontFamily: 'Cormorant SC',
              fontWeight: 700,
              textTransform: 'uppercase',
              lineHeight: 1.3,
            }}
          >
            Your Favorite Handbags
          </h1>
          <p
            className="max-w-2xl mb-6 font-light lg:mb-8 md:text-lg lg:text-xl"
            style={{
              color: '#675B5B',
              fontSize: 18,
              fontWeight: 400,
              lineHeight: '28px',
            }}
          >
            Discover Quality and Style in Our Exceptional
            <br />
            Leather Collection for Women
          </p>
          <button
            type="button"
            onClick={() => router.push('/product/listing/all-products')}
            className="mt-1.5 inline-block px-8 py-3 text-s font-bold tracking-wide text-white"
            style={{
              background: '#7A001A',
              borderRadius: 6,
            }}
          >
            View all Product
          </button>
        </div>
        <div className="hidden lg:block lg:col-span-6 lg:mt-3 lg:mr-3 lg:ml-8 mt-10 relative">
          <img
            src="https://s3-alpha-sig.figma.com/img/57e8/89ef/eb26415731d477f61e79942f661192be?Expires=1701648000&Signature=hnpAG-Axl2M4GGQbGxyZD2AZs3v12n4PkqWKxbtUjMwO7fPq1zwMpPSZ1ye9TN1JfVBzW8oMSdJPtzsNweRvZAqWRgNij8EO8ykCQTd-2gw1ga96i0pb1Ovfyo1FhHB4BVWTzkDhhHgp0nXmLJMCsF~BvSJz4v435wtltl4p1K05Roqr97o0RgAbsAcxznjb1YJXvU~sISZNRJ2Xo4lkjMd4R7U7JEufYzZTZXq4x7dsKfXS~SrEuPHqn5GEriBcfNyuKlGtQtnPI7wF3lbZXc-kYK~dTt4L9xIx2RLXoHfoyF6-IUMv-0~8bZASU6IKs6jwBooOgu2rkgo2l390DQ__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
            className="w-full h-auto absolute top-0 left-0 mt-[-22%]"
          />
        </div>
      </div>
    </main>
  );
}
