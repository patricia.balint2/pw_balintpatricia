import Link from 'next/link';

function CategoryCard({ route, imageSrc, categoryName }) {
  return (
    <div className="relative block group">
      <img
        src={imageSrc}
        className="object-cover w-full rounded h-80"
        alt={categoryName}
      />
      <div className="absolute inset-0 flex flex-col items-start justify-end p-6">
        <h3 className="text-xl font-medium text-white">{categoryName}</h3>
        <Link href={route}>
          <div className="mt-1.5 inline-block bg-black px-2 py-2 text-xs font-medium uppercase tracking-wide text-white cursor-pointer rounded">
            View Products
          </div>
        </Link>
      </div>
    </div>
  );
}

export default function Categories() {
  return (
    <section className="bg-white py-20 sm:py-16 h-screen">
      <div className="max-w-screen-xl px-4 py-8 mx-auto sm:px-6 sm:py-3 lg:px-8">
        <div className="text-center">
          <h2 className="text-xl font-bold sm:text-3xl">
            <div
              style={{
                color: '#250A0A',
                fontFamily: 'Cormorant SC',
              }}
            >
              SHOP BY CATEGORY
            </div>
          </h2>
        </div>
        <ul className="grid grid-cols-1 gap-4 mt-12 lg:grid-cols-4">
          <li>
            <CategoryCard
              route="/product/listing/category1"
              imageSrc="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fcategory1.png?alt=media&token=5f38c455-c719-4d35-9a12-aae83d6504f7"
              categoryName="Hand Bags"
            />
          </li>
          <li>
            <CategoryCard
              route="/product/listing/category2"
              imageSrc="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fcat2.png?alt=media&token=13d75be4-6886-450b-b64a-4dd07589ad65"
              categoryName="Sling Bags"
            />
          </li>
          <li>
            <CategoryCard
              route="/product/listing/category3"
              imageSrc="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fcart3.png?alt=media&token=02e0c6cc-f022-40ce-ad7a-3c824438940f"
              categoryName="Tote Bags"
            />
          </li>
          <li>
            <CategoryCard
              route="/product/listing/category4"
              imageSrc="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fcart4.png?alt=media&token=779cf19d-43ef-4ab4-a831-6af29fbc8871"
              categoryName="Clutch Bags"
            />
          </li>
        </ul>
      </div>
    </section>
  );
}
