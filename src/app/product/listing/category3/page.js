import CommonListing from '@/components/CommonListing';
import { productByCategory } from '@/services/product';

export default async function clutchbagsProducts() {
  const getAllProducts = await productByCategory('totebags');

  return <CommonListing data={getAllProducts && getAllProducts.data} />;
}
