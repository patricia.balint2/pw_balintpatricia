import CommonListing from '@/components/CommonListing';
import { productByCategory } from '@/services/product';

export default async function HandBagsAllProducts() {
  const getAllProducts = await productByCategory('Hand Bags');

  return <CommonListing data={getAllProducts && getAllProducts.data} />;
}
