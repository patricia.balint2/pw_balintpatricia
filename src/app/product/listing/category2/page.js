import CommonListing from '@/components/CommonListing';
import { productByCategory } from '@/services/product';

export default async function SlingbagsProducts() {
  const getAllProducts = await productByCategory('slingbags');

  return <CommonListing data={getAllProducts && getAllProducts.data} />;
}
