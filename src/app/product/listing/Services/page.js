import React from 'react';

const ServicesPage = () => {
  return (
    <main
      className="h-screen w-screen flex flex-col items-center justify-center"
      style={{
        backgroundImage:
          "url('https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fimg2.jpg?alt=media&token=29847943-12a1-497f-b8be-436e13017dc7')",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
      }}
    >
      <div className="max-w-2xl mt-[-200px] text-center">
        <div
          className="text-4xl md:text-5xl xl:text-6xl"
          style={{
            color: '#250A0A',
            fontSize: 34,
            fontFamily: 'Cormorant SC',
            textTransform: 'uppercase',
          }}
        >
          WE WANT YOU TO EXPRESS YOURSELF
        </div>
        <div className="flex mt-16 justify-center">
          <div className="bg-white bg-opacity-50 rounded-lg p-12 shadow-md mr-8 flex items-center justify-center flex-col">
            <div className="mb-3">
              <img
                src="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2FCurrency%20Dollar.svg?alt=media&token=13534afc-1485-4015-a266-bc1aacbcd295"
                alt="Currency Dollar"
              />
            </div>
            <div
              className="mb-3"
              style={{
                color: '#250A0A',
                fontSize: 24,
                fontFamily: 'Montserrat',
                fontWeight: '600',
              }}
            >
              Refund Policy
            </div>
            <div
              style={{
                width: 274,
                textAlign: 'center',
                color: '#675B5B',
                fontSize: 16,
                fontFamily: 'Montserrat',
                fontWeight: '400',
              }}
            >
              Changed your mind? No problem! You are covered by our 30-day
              refund policy
            </div>
          </div>
          <div className="bg-white bg-opacity-50 rounded-lg p-12 shadow-md mr-8 flex items-center justify-center flex-col">
            <div className="mb-3">
              <img
                src="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fpepicons-pencil_gift-circle.svg?alt=media&token=292f9d98-f08e-4543-977b-25d9a8fe1118"
                alt="Currency Dollar"
              />
            </div>
            <div
              className="mb-3"
              style={{
                color: '#250A0A',
                fontSize: 24,
                fontFamily: 'Montserrat',
                fontWeight: '600',
              }}
            >
              Premium packaging
            </div>
            <div
              style={{
                width: 274,
                textAlign: 'center',
                color: '#675B5B',
                fontSize: 16,
                fontFamily: 'Montserrat',
                fontWeight: '400',
              }}
            >
              All pieces come carefully packed complete with dust bags and
              packaging so gorgeous you will want to
            </div>
          </div>
          <div className=" bg-white bg-opacity-50 rounded-lg p-12 shadow-md mr-8 flex items-center justify-center flex-col">
            <div className="mb-3">
              <img
                src="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2FCheck%20circle.svg?alt=media&token=aa4f7d97-aef1-43b3-a00e-1422fa6a1444"
                alt="Currency Dollar"
              />
            </div>
            <div
              className="mb-3"
              style={{
                color: '#250A0A',
                fontSize: 24,
                fontFamily: 'Montserrat',
                fontWeight: '600',
              }}
            >
              Superior quality
            </div>
            <div
              style={{
                width: 274,
                textAlign: 'center',
                color: '#675B5B',
                fontSize: 16,
                fontFamily: 'Montserrat',
                fontWeight: '400',
              }}
            >
              We take great care to ensure the best quality leathers are used on
              all our pieces.
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default ServicesPage;
