import CommonListing from '@/components/CommonListing';
import { productByCategory } from '@/services/product';

export default async function TotebagsProducts() {
  const getAllProducts = await productByCategory('clutchbags');

  return <CommonListing data={getAllProducts && getAllProducts.data} />;
}
