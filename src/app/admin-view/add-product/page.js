'use client';

import InputComponent from '@/components/FormElements/InputComponent';
import SelectComponent from '@/components/FormElements/SelectComponent';
import TileComponent from '@/components/FormElements/TileComponent';
import ComponentLevelLoader from '@/components/Loader/componentlevel';
import Notification from '@/components/Notification';
import { GlobalContext } from '@/context';
import { addNewProduct, updateAProduct } from '@/services/product';
import {
  AvailableColors,
  adminAddProductformControls,
  firebaseConfig,
  firebaseStroageURL,
} from '@/utils';
import {
  getDownloadURL,
  getStorage,
  ref,
  uploadBytesResumable,
} from 'firebase/storage';
import { useRouter } from 'next/navigation';
import { useContext, useEffect, useState } from 'react';
import { initializeApp } from 'firebase/app';
import { toast } from 'react-toastify';

const app = initializeApp(firebaseConfig);
const storage = getStorage(app, firebaseStroageURL);

const createUniqueFileName = (getFile) => {
  const timeStamp = Date.now();
  const randomStringValue = Math.random().toString(36).substring(2, 12);

  return `${getFile.name}-${timeStamp}-${randomStringValue}`;
};

async function helperForUPloadingImageToFirebase(file) {
  const getFileName = createUniqueFileName(file);
  const storageReference = ref(storage, `projectpw/${getFileName}`);
  const uploadImage = uploadBytesResumable(storageReference, file);

  return new Promise((resolve, reject) => {
    uploadImage.on(
      'state_changed',
      (snapshot) => {},
      (error) => {
        console.log(error);
        reject(error);
      },
      () => {
        getDownloadURL(uploadImage.snapshot.ref)
          .then((downloadUrl) => resolve(downloadUrl))
          .catch((error) => reject(error));
      }
    );
  });
}

const initialFormData = {
  name: '',
  price: 0,
  description: '',
  category: 'Hand Bags',
  colors: [],
  deliveryInfo: '',
  onSale: 'no',
  imageUrl: '',
  priceDrop: 0,
};

export default function AdminAddNewProduct() {
  const [formData, setFormData] = useState(initialFormData);

  const {
    componentLevelLoader,
    setComponentLevelLoader,
    currentUpdatedProduct,
    setCurrentUpdatedProduct,
  } = useContext(GlobalContext);
  console.log(currentUpdatedProduct);
  const router = useRouter();

  useEffect(() => {
    if (currentUpdatedProduct !== null) setFormData(currentUpdatedProduct);
  }, [currentUpdatedProduct]);

  async function handleImage(event) {
    const extractImageUrl = await helperForUPloadingImageToFirebase(
      event.target.files[0]
    );

    if (extractImageUrl !== '') {
      setFormData({
        ...formData,
        imageUrl: extractImageUrl,
      });
    }
  }

  function handleTileClick(getCurrentItem) {
    let updatedColors = [...formData.colors];
    const colorExists = updatedColors.some(
      (item) => item.id === getCurrentItem.id
    );

    if (!colorExists) {
      const selectedColor = AvailableColors.find(
        (color) => color.id === getCurrentItem.id
      );
      if (selectedColor) {
        updatedColors.push(selectedColor);
      }
    } else {
      updatedColors = updatedColors.filter(
        (item) => item.id !== getCurrentItem.id
      );
    }

    setFormData({
      ...formData,
      colors: updatedColors,
    });
    const ColorOptions = () => {
      return (
        <div className="flex gap-2">
          {AvailableColors.map((color) => (
            <div
              key={color.id}
              style={{
                width: '50px',
                height: '50px',
                backgroundColor: color.color, // Utilizăm codul real de culoare pentru a afișa culoarea
                margin: '5px',
                cursor: 'pointer',
              }}
              onClick={() => handleTileClick(color)}
            ></div>
          ))}
        </div>
      );
    };
  }

  async function handleAddProduct() {
    setComponentLevelLoader({ loading: true, id: '' });
    const res =
      currentUpdatedProduct !== null
        ? await updateAProduct(formData)
        : await addNewProduct(formData);

    console.log(res);

    if (res.success) {
      setComponentLevelLoader({ loading: false, id: '' });
      toast.success(res.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      setFormData(initialFormData);
      setCurrentUpdatedProduct(null);
      setTimeout(() => {
        router.push('/admin-view/all-products');
      }, 1000);
    } else {
      toast.error(res.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      setComponentLevelLoader({ loading: false, id: '' });
      setFormData(initialFormData);
    }
  }

  console.log(formData);

  return (
    <div className="bg-white py-20 sm:py-16 h-screen">
      <div className="flex flex-col items-start justify-start p-10 bg-white shadow-2xl rounded-xl relative">
        <div className="w-full mt-6 mr-0 mb-0 ml-0 space-y-8">
          <input
            accept="image/*"
            max="1000000"
            type="file"
            onChange={handleImage}
          />
          <div className="flex gap-2 flex-col">
            <label>Available colors</label>
            <TileComponent
              selected={formData.colors}
              onClick={handleTileClick}
              data={AvailableColors}
            />
          </div>
          {adminAddProductformControls.map((controlItem) =>
            controlItem.componentType === 'input' ? (
              <InputComponent
                key={controlItem.id}
                type={controlItem.type}
                placeholder={controlItem.placeholder}
                label={controlItem.label}
                value={formData[controlItem.id]}
                onChange={(event) => {
                  setFormData({
                    ...formData,
                    [controlItem.id]: event.target.value,
                  });
                }}
              />
            ) : controlItem.componentType === 'select' ? (
              <SelectComponent
                label={controlItem.label}
                options={controlItem.options}
                value={formData[controlItem.id]}
                onChange={(event) => {
                  setFormData({
                    ...formData,
                    [controlItem.id]: event.target.value,
                  });
                }}
              />
            ) : null
          )}
          <button
            onClick={handleAddProduct}
            className="inline-flex w-full items-center justify-center px-6 py-4 text-lg text-white font-medium tracking-wide"
            style={{
              background: '#7A001A',
              borderRadius: 6,
            }}
          >
            {componentLevelLoader && componentLevelLoader.loading ? (
              <ComponentLevelLoader
                text={
                  currentUpdatedProduct !== null
                    ? 'Updating Product'
                    : 'Adding Product'
                }
                color={'#ffffff'}
                loading={componentLevelLoader && componentLevelLoader.loading}
              />
            ) : currentUpdatedProduct !== null ? (
              'Update Product'
            ) : (
              'Add Product'
            )}
          </button>
        </div>
      </div>
      <Notification />
    </div>
  );
}
