'use client';

import { GlobalContext } from '@/context';
import { adminNavOptions, navOptions } from '@/utils';
import { Fragment, useContext, useEffect } from 'react';
import { usePathname, useRouter } from 'next/navigation';
import CommonModal from '../CommonModal';
import Cookies from 'js-cookie';
import CartModal from '../CartModal';

function NavItems({ isModalView = false, isAdminView, router }) {
  return (
    <div
      className={`items-center justify-between w-full md:flex md:w-auto ${
        isModalView ? '' : 'hidden'
      }`}
      id="nav-items"
    >
      <ul
        className={`flex flex-col p-4 md:p-0 mt-4 font-medium  rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 bg-white ${
          isModalView ? 'border-none' : 'border border-gray-100'
        }`}
      >
        {isAdminView
          ? adminNavOptions.map((item) => (
              <li
                className="cursor-pointer block py-2 pl-3 pr-4 text-gray-900 rounded md:p-0"
                key={item.id}
                onClick={() => router.push(item.path)}
              >
                {item.label}
              </li>
            ))
          : navOptions.map((item) => (
              <li
                className="cursor-pointer block py-2 pl-3 pr-4 text-gray-900 rounded md:p-0"
                key={item.id}
                onClick={() => router.push(item.path)}
              >
                {item.label}
              </li>
            ))}
      </ul>
    </div>
  );
}

export default function Navbar() {
  const { showNavModal, setShowNavModal } = useContext(GlobalContext);
  const {
    user,
    isAuthUser,
    setIsAuthUser,
    setUser,
    currentUpdatedProduct,
    setCurrentUpdatedProduct,
    showCartModal,
    setShowCartModal,
  } = useContext(GlobalContext);

  const pathName = usePathname();
  const router = useRouter();

  console.log(currentUpdatedProduct, 'navbar');
  useEffect(() => {
    if (
      pathName !== '/admin-view/add-product' &&
      currentUpdatedProduct !== null
    )
      setCurrentUpdatedProduct(null);
  }, [pathName]);

  function handleLogout() {
    setIsAuthUser(false);
    setUser(null);
    Cookies.remove('token');
    localStorage.clear();
    router.push('/');
  }

  const isAdminView = pathName.includes('admin-view');

  return (
    <>
      <nav className="bg-white fixed w-full z-20 top-0 left-0 border-b border-gray-200">
        <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
          <div
            onClick={() => router.push('/')}
            className="flex items-center cursor-pointer"
          >
            <span className="slef-center text-2xl font-semibold whitespace-nowrap">
              <div
                style={{
                  color: '#613C3C',
                  fontSize: 24,
                  fontFamily: 'Cinzel Decorative',
                }}
              >
                Glamour
              </div>
            </span>
          </div>
          <div className="flex md:order-2 space-x-1">
            {!isAdminView && isAuthUser ? (
              <Fragment>
                <button
                  className="mt-1.5 inline-block px-3 py-2 tracking-wide"
                  onClick={() => router.push('/account')}
                >
                  <img
                    src="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Faccount.svg?alt=media&token=0178ef8f-d926-49c8-85c3-0dd18ab3586f"
                    alt="Account Icon"
                    width="22"
                    height="22"
                  />
                </button>
                <button
                  className="mt-1.5 inline-block px-3 py-2 tracking-wide"
                  onClick={() => setShowCartModal(true)}
                >
                  <img
                    src="https://firebasestorage.googleapis.com/v0/b/projectpw-c4712.appspot.com/o/projectpw%2Fbag.svg?alt=media&token=fea444f4-97cb-4b6b-8f3b-0c5f22e5df86"
                    alt="Cart Icon"
                    width="25"
                    height="25"
                  />
                </button>
              </Fragment>
            ) : null}
            {user?.role === 'admin' ? (
              isAdminView ? (
                <button
                  className="mt-1.5 inline-block px-3 py-2 tracking-wide"
                  onClick={() => router.push('/')}
                  style={{
                    background: '#7A001A',
                    borderRadius: 6,
                    whiteSpace: 'nowrap',
                  }}
                >
                  <span
                    style={{
                      color: '#F9F9F9',
                      fontSize: 14,
                      fontFamily: 'Montserrat, sans-serif',
                      fontWeight: '500',
                    }}
                  >
                    Client View
                  </span>
                </button>
              ) : (
                <button
                  onClick={() => router.push('/admin-view')}
                  className="mt-1.5 inline-block px-3 py-2 tracking-wide"
                  style={{
                    background: '#7A001A',
                    borderRadius: 6,
                    whiteSpace: 'nowrap',
                  }}
                >
                  <span
                    style={{
                      color: '#F9F9F9',
                      fontSize: 14,
                      fontFamily: 'Montserrat, sans-serif',
                      fontWeight: '500',
                    }}
                  >
                    Admin View
                  </span>
                </button>
              )
            ) : null}
            {isAuthUser ? (
              <button
                onClick={handleLogout}
                className="mt-1.5 inline-block px-3 py-2 tracking-wide"
                style={{
                  background: '#7A001A',
                  borderRadius: 6,
                }}
              >
                <span
                  style={{
                    color: '#F9F9F9',
                    fontSize: 14,
                    fontFamily: 'Montserrat, sans-serif',
                    fontWeight: '500',
                  }}
                >
                  Logout
                </span>
              </button>
            ) : (
              <button
                onClick={() => router.push('/login')}
                className="mt-1.5 inline-block px-3 py-2 tracking-wide tracking-wide text-white text-base font-medium font-montserrat sans-serif"
                style={{
                  background: '#7A001A',
                  borderRadius: 6,
                }}
              >
                Log in
              </button>
            )}
            <button
              data-collapse-toggle="navbar-sticky"
              type="button"
              className="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              aria-controls="navbar-sticky"
              aria-expanded="false"
              onClick={() => setShowNavModal(true)}
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="w-6 h-6"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </button>
          </div>
          <NavItems router={router} isAdminView={isAdminView} />
        </div>
      </nav>
      <CommonModal
        showModalTitle={false}
        mainContent={
          <NavItems
            router={router}
            isModalView={true}
            isAdminView={isAdminView}
          />
        }
        show={showNavModal}
        setShow={setShowNavModal}
      />
      {showCartModal && <CartModal />}
    </>
  );
}
