export default function TileComponent({ data, selected = [], onClick }) {
  return data && data.length ? (
    <div className="mt-3 flex flex-wrap items-center gap-1">
      {data.map((dataItem) => (
        <label
          onClick={() => onClick(dataItem)}
          key={dataItem.id}
          className={`cursor-pointer`}
        >
          <span
            className={`rounded-lg border px-6 py-2 font-bold`}
            style={{
              backgroundColor: selected.some((item) => item.id === dataItem.id)
                ? dataItem.color // Utilizează codul real de culoare pentru a afișa culoarea
                : '',
              color: selected.some((item) => item.id === dataItem.id)
                ? '#ffffff' // Schimbă culoarea textului dacă opțiunea este selectată
                : '',
            }}
          >
            {
              selected.some((item) => item.id === dataItem.id)
                ? '' // Dacă este selectată o opțiune de culoare, nu afișa text
                : dataItem.label // Altfel, afișează eticheta culorii
            }
          </span>
        </label>
      ))}
    </div>
  ) : null;
}
