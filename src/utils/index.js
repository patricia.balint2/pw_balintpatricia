export const navOptions = [
  {
    id: 'home',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Home
      </span>
    ),
    path: '/',
  },
  {
    id: 'listing',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Categories
      </span>
    ),
    path: '/product/listing/categories',
  },
  {
    id: 'listingAllProducts',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Products
      </span>
    ),
    path: '/product/listing/all-products',
  },
  {
    id: 'listingServices',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Services
      </span>
    ),
    path: '/product/listing/Services',
  },
];

export const adminNavOptions = [
  {
    id: 'adminListing',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Manage All Products
      </span>
    ),
    path: '/admin-view/all-products',
  },
  {
    id: 'adminNewProduct',
    label: (
      <span
        style={{
          color: '#675B5B',
          fontSize: 16,
          fontFamily: 'Montserrat',
          wordWrap: 'break-word',
        }}
      >
        Add new product
      </span>
    ),
    path: '/admin-view/add-product',
  },
];

export const registrationFormControls = [
  {
    id: 'name',
    type: 'text',
    placeholder: 'Enter your name',
    label: 'Name',
    componentType: 'input',
  },
  {
    id: 'email',
    type: 'email',
    placeholder: 'Enter your email',
    label: 'Email',
    componentType: 'input',
  },
  {
    id: 'password',
    type: 'password',
    placeholder: 'Enter your password',
    label: 'Password',
    componentType: 'input',
  },
  {
    id: 'role',
    type: '',
    placeholder: '',
    label: 'Role',
    componentType: 'select',
    options: [
      {
        id: 'admin',
        label: 'Admin',
      },
      {
        id: 'customer',
        label: 'customer',
      },
    ],
  },
];

export const loginFormControls = [
  {
    id: 'email',
    type: 'email',
    placeholder: 'Enter your email',
    label: 'Email',
    componentType: 'input',
  },
  {
    id: 'password',
    type: 'password',
    placeholder: 'Enter your password',
    label: 'Password',
    componentType: 'input',
  },
];

export const adminAddProductformControls = [
  {
    id: 'name',
    type: 'text',
    placeholder: 'Enter name',
    label: 'Name',
    componentType: 'input',
  },
  {
    id: 'price',
    type: 'number',
    placeholder: 'Enter price',
    label: 'Price',
    componentType: 'input',
  },
  {
    id: 'description',
    type: 'text',
    placeholder: 'Enter description',
    label: 'Description',
    componentType: 'input',
  },
  {
    id: 'category',
    type: '',
    placeholder: '',
    label: 'Category',
    componentType: 'select',
    options: [
      {
        id: 'handbags',
        label: 'Hand Bags',
      },
      {
        id: 'slingbags',
        label: 'Sling Bags',
        path: '/product/listing/category2',
      },
      {
        id: 'totebags',
        label: 'Tote bags',
        path: '/product/listing/category3',
      },
      {
        id: 'clutchbags',
        label: 'Clutch Bags',
        path: '/product/listing/category1',
      },
    ],
  },
  {
    id: 'deliveryInfo',
    type: 'text',
    placeholder: 'Enter deliveryInfo',
    label: 'Delivery Info',
    componentType: 'input',
  },
  {
    id: 'onSale',
    type: '',
    placeholder: '',
    label: 'On Sale',
    componentType: 'select',
    options: [
      {
        id: 'yes',
        label: 'Yes',
      },
      {
        id: 'no',
        label: 'No',
      },
    ],
  },
  {
    id: 'priceDrop',
    type: 'number',
    placeholder: 'Enter Price Drop',
    label: 'Price Drop',
    componentType: 'input',
  },
];

export const AvailableColors = [
  {
    id: 'dark',
    label: 'Dark',
    color: '#0A0909',
  },
  {
    id: 'white',
    label: 'White',
    color: '#fff',
  },
  {
    id: 'gold',
    label: 'Gold',
    color: '#CFBFA2',
  },
  {
    id: 'cherry',
    label: 'Cherry',
    color: '#7A001A',
  },
];

export const firebaseConfig = {
  apiKey: 'AIzaSyCIad2JeC7-qNDqeOqZd0ve20CK4zJGl3w',
  authDomain: 'projectpw-c4712.firebaseapp.com',
  projectId: 'projectpw-c4712',
  storageBucket: 'projectpw-c4712.appspot.com',
  messagingSenderId: '755909286766',
  appId: '1:755909286766:web:ed6acb1780ec235e55bf7f',
  measurementId: 'G-FZCG96M6QF',
};
export const firebaseStroageURL = 'gs://projectpw-c4712.appspot.com';

export const addNewAddressFormControls = [
  {
    id: 'fullName',
    type: 'input',
    placeholder: 'Enter your full name',
    label: 'Full Name',
    componentType: 'input',
  },
  {
    id: 'address',
    type: 'input',
    placeholder: 'Enter your full address',
    label: 'Address',
    componentType: 'input',
  },
  {
    id: 'city',
    type: 'input',
    placeholder: 'Enter your city',
    label: 'City',
    componentType: 'input',
  },
  {
    id: 'country',
    type: 'input',
    placeholder: 'Enter your country',
    label: 'Country',
    componentType: 'input',
  },
  {
    id: 'postalCode',
    type: 'input',
    placeholder: 'Enter your postal code',
    label: 'Postal Code',
    componentType: 'input',
  },
];
